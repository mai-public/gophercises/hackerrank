package main

/*
-----------------------------------------------------------
- https://www.hackerrank.com/challenges/camelcase/problem -
-----------------------------------------------------------
*/

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"unicode"
)

//This is our problem. This function needs to take a camelCase string and return the amount of words.
func camelcase(s string) int32 {
	var words int32 = 1
	if s == "" {
		return 0
	}

	for _, l := range s {
		if unicode.IsUpper(l) {
			words++
		}

	}
	return words

}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 1024*1024)

	stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	checkError(err)

	defer stdout.Close()

	writer := bufio.NewWriterSize(stdout, 1024*1024)

	s := readLine(reader)

	result := camelcase(s)

	fmt.Fprintf(writer, "%d\n", result)

	writer.Flush()
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
