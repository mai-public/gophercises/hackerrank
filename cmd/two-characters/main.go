package main

import (
	"fmt"
)

func alternate(s string) int32 {
	type letter struct {
		l       string
		indexes []int
	}
	m := make(map[string]letter)
	// Get all unique numbers
	for i, l := range s {
		l := string(l)
		if e, ok := m[l]; ok {
			m[l] = letter{e.l, append(e.indexes, i)}
		} else {
			m[l] = letter{
				l,
				[]int{i},
			}
		}
	}
	if len(m) < 2 {
		return int32(0)
	}
	letters := make([]letter, len(m))
	i := 0
	for _, v := range m {
		//log.Println(v, i)
		letters[i] = v
		i++
	}

	type maxRepeated struct {
		ls []string
		v  int32
	}
	//log.Println(letters)
	//log.Println(m)
	n := 1
	max := &maxRepeated{[]string{"", ""}, -1}
	for _, l1 := range letters {
		for _, l2 := range letters[n:] {
			timesRepeated := 0
			iter1 := 0
			iter2 := 0
			max1 := len(l1.indexes)
			max2 := len(l2.indexes)
			currl := 0
			for {
				//log.Printf("1(%s, %d, %d) 2(%s, %d, %d) timesRepeated(%d) currl(%d)", l1.l, iter1, max1, l2.l, iter2, max2, timesRepeated, currl)
				if iter1 == max1 {
					if currl != 2 {
						timesRepeated++
					}
					break
				}
				if iter2 == max2 {
					if currl != 1 {
						timesRepeated++
					}
					break
				}
				index1 := l1.indexes[iter1]
				index2 := l2.indexes[iter2]
				if index1 < index2 {
					if currl != 1 {
						timesRepeated++
						currl = 1
					}
					iter1++
				} else if index1 > index2 {
					if currl != 2 {
						timesRepeated++
						currl = 2
					}
					iter2++
				}
			}
			if int32(timesRepeated) > max.v {
				max = &maxRepeated{[]string{l1.l, l2.l}, int32(timesRepeated)}
			}
		}
		n++
	}
	// remove all concurrently repeating letters
	// iterate through all letters
	fmt.Println(max)
	return max.v
}

func main() {
	s := "asvkugfiugsalddlasguifgukvsa" //"beabeefeab""ezfnjymgqtjnmstbadgdsrxvntnacwljnkgchtjeaoivfcindgxipmrjuqmmcpntpotplodjhijxqpogjmzipygacfdjgmewechuebxvcbnakszzcxkozxwavzgmesrvysonomhvufezislfntgncspthcpneyminpbjildobozfirvcgdratdpmmpkujcywvtzkdytzyfejbytsobvudvutfueveevgrqnxjiwpkrvllsjxmqhotlnpgjxkjnobxfqodlyiqsisdeuwqmntxouzdtisgutdafostmwticvncjwldpknuodmfksusaqpsoosgpiveyxipfklmhypdxpdncpgaswpycoxsuxasqduojpblctcyvyxldcgzevedvxiwinfppkjbtifuuapickknwxxjmjmtxlpfalxdgepmekaxijuphqfafrnezyldokwcnzenhpibktlfuxjfmeqajmvopbhuslnnnlmkmoteceiwbytjhhxqnkuazevswrkaofggfrnapciuoexqogscugzspwuvzkyrdfkhixcaqctfwadewpqksxxvqiigvjjpagvqikuojlwhfyztu" //"beabeefeab"
	fmt.Println(len(s))
	result := alternate(s)
	fmt.Println(result)
}
