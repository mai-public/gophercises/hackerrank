package main

import (
	"fmt"
	"strings"
	"unicode"
)

// Complete the minimumNumber function below.
func minimumNumber(n int32, password string) int32 {
	// Return the minimum number of characters to make the password strong
	minN := int32(6)
	minNeeded := minN - n
	if minNeeded >= 3 {
		return minNeeded
	}
	var stillNeeded int32 = 4

	hNum := false
	hSpc := false
	hUpp := false
	hLow := false
	special_characters := "!@#$%^&*()-+"

	for _, c := range password {
		if !hNum {
			if unicode.IsDigit(c) {
				hNum = true
				stillNeeded--
			}

		}
		if !hUpp {
			if unicode.IsUpper(c) {
				hUpp = true
				stillNeeded--
			}

		}
		if !hLow {
			if unicode.IsLower(c) {
				hLow = true
				stillNeeded--
			}

		}
		if !hSpc {
			if strings.ContainsRune(special_characters, c) {
				hSpc = true
				stillNeeded--
			}

		}

	}
	if minNeeded < 0 {
		return stillNeeded
	} else if stillNeeded > minNeeded {
		return stillNeeded

	} else {
		return minNeeded
	}

}

func main() {
	//reader := bufio.NewReaderSize(os.Stdin, 1024 * 1024)

	//stdout, err := os.Create(os.Getenv("OUTPUT_PATH"))
	//checkError(err)

	//defer stdout.Close()

	//writer := bufio.NewWriterSize(stdout, 1024 * 1024)
	pwd := "12"
	//nTemp, err := strconv.ParseInt(readLine(reader), 10, 64)
	//checkError(err)

	//n := int32(nTemp)
	n := int32(len(pwd))

	password := pwd

	answer := minimumNumber(n, password)
	fmt.Printf("%d\n", answer)
	//    fmt.Fprintf(writer, "%d\n", answer)

	//    writer.Flush()
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
